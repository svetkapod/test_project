package com.sv;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class APITest {
    @Test
    public void checkCity () {
        String name = "moscow";

        given()
            .log().all()
            .pathParam("name", name)
            .when()
            .get("https://restcountries.eu/rest/v2/capital/{name}")
            .then()
            .log().all()
            .statusCode(200)
            .body("[0].name", equalTo("Russian Federation"))
            .extract()
            .response();
    }
}
