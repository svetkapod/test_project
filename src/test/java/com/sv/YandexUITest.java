package com.sv;

import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import org.junit.Assert;
import org.junit.Test;

public class YandexUITest {

    @Test
    public void testCheckLinks() {
        // настроили
        Configuration.browser = "chrome";
        Configuration.timeout = 10000;
        open("http://yandex.ru/news/");

        // иногда тест падает
        // Яндекс меняет верстку и меняются селекторы
        // поэтому выбранные эелементы не ищутся
        ElementsCollection resultLinks = $$(".tabs-menu_main >li:nth-child(n+1) a");

        // проверили количество
        //System.out.println("size = " + resultLinks.size());
        resultLinks.shouldBe(CollectionCondition.size(14));

        // переходим по ссылкам
        String[] list = {
            "Главное",
            "Коронавирус",
            "Воронеж", // может быть другой регион
            "Интересное",
            "Политика",
            "Общество",
            "Экономика",
            "В мире",
            "Происшествия",
            "Культура",
            "Технологии",
            "Наука",
            "Авто",
            //"Спорт", // тут открывается новый таб и можно\нужно проверить url уже в нем
        };
        for (int i = 0; i < list.length; i++) {
            resultLinks.findBy(Condition.text(list[i])).click();
        }

        // проверим что после клика отображается корректная ссылка
        String currentUrl = url();
        System.out.println("currentUrl = " + currentUrl);
        Assert.assertEquals("https://yandex.ru/news/rubric/auto?from=rubric", currentUrl);
    }
}
